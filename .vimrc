" Gvim profile
" Author: Derek Saw
" Last Modified: 2011-07-26 02:09:47 PM
" See Also: http://xolox.ath.cx/vim/vimrc

" Disable Vi compatibility
set nocompatible

" Enable file type detection and (indent) plug-ins
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
filetype on
filetype plugin indent on

" Some global settings
let g:CommandTMaxFiles=30000
let g:SuperTabDefaultCompletionType="context"
let g:ctrlp_working_path_mode=0
let g:pyflakes_use_quickfix=0
let g:python_highlight_all=1
let g:cpp_no_function_highlight=1

" color scheme settings
"let ayucolor="dark"
"let ayucolor="light"
let g:ayucolor="mirage"
let g:gruvbox_italic=1
let g:molokai_original=1
let g:monokai_gui_italic = 1
let g:monokai_term_italic = 1
let g:one_allow_italics=1
let g:palenight_terminal_italics=1
let g:solarized_bold=1
let g:solarized_contrast="high"
let g:solarized_italic=1
"let g:solarized_termcolors=256
"let g:solarized_visibility="normal"
let g:gruvbox_material_background='medium'
let g:gruvbox_material_enable_italic=1
let g:gruvbox_material_disable_italic_comment=0
let g:edge_style='neon'
let g:edge_menu_seleection_background='purple'
"let g:edge_transparent_background=1
let g:one_allow_italics=0


let g:ycm_enable_diagnostic_signs=1
let g:ycm_enable_diagnostic_highlighting=1
let g:ycm_always_populate_location_list=1 " default 0
let g:ycm_open_loclist_on_ycm_diags=1     " default 1
let g:ycm_seed_identifiers_with_syntax=1
let g:ycm_min_num_of_chars_for_completion=3

nnoremap <F11> :YcmForceCompileAndDiagnostics <CR>

let g:ycm_python_binary_path='/usr/bin/python3'
let g:ycm_confirm_extra_conf=0
let g:ycm_error_symbol='x'
"let g:ycm_error_symbol='✗'
"let g:ycm_warning_symbol='▲'
let g:ycm_warning_symbol='>'
"let g:zenburn_high_Contrast=0
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
let g:syntastic_cpp_check_header=1
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list=1
let g:syntastic_reuse_loc_lists=1
let g:syntastic_loc_list_height=5
let g:syntastic_check_on_open=0
let g:syntastic_check_on_wq=1
let g:syntastic_python_checkers=['flake8']
let g:syntastic_quiet_messages={ "type": "style" }
let g:syntastic_cpp_config_file='.clang_complete'

" file type specific
autocmd FileType python setlocal sw=4 sts=4 ts=4 et omnifunc=pythoncomplete#Complete
autocmd FileType html setlocal sw=2 sts=2 ts=2 et
autocmd FileType htm setlocal sw=2 sts=2 ts=2 et
autocmd FileType css setlocal sw=2 sts=2 ts=2 et
autocmd FileType vimrc setlocal sw=4 sts=4 ts=4 et

set wildignore+=*.o,*.obj,*.pyc,*.class,*.war,*.jar,*.iml,*.ipr,*.iws,*.a,*.d,*.gcno,*.gcda
"set completeopt=menuone,longest,preview
"let g:loaded_netrwPlugin = 1
"set browsedir=buffer

set ff=unix

" vim plugin: pathogen infect
call pathogen#infect()


" Enable syntax highlighting & load color scheme
syntax on
set termguicolors
set t_Co=256


" highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\(\s\|\%u00a0\)\+$/
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red

" highlight leading mixed tab/space
highlight MixTabSpace ctermbg=brown guibg=orange
match MixTabSpace /^\(\t\+ \+\| \+\t\+\)\t* */
autocmd ColorScheme * highlight MixTabSpace ctermbg=brown guibg=orange

highlight Normal ctermbg=NONE

" Enable Windows key mappings (Ctrl-C, Ctrl-V, etc.)
"runtime mswin.vim


" Start browsing from the directory of the current buffer
set browsedir=buffer


" Hide the menu, tool-bar and right and side scrollbar
set go-=T go-=r go-=l go-=R go-=L


" Show textual tab line
set go-=e


" Open up to 25 open tab pages
set tabpagemax=25


" Refresh the pop-up tool tip every 45 ms.
"set balloondelay=45

" Smaller insert and command cursors
"set guicursor+=i-ci:ver10-Cursor/lCursor

" Smaller visual and select mode cursors
"set guicursor+=ve:ver10-Cursor

" favorite font
if has('gui_win32')
    set dir=$TEMP,$TMP
    set undodir=$TEMP,$TMP
    "set guifont=ProggyCleanSZBP:h8:cANSI
    "set guifont=Consolas:h10
    set guifont=Envy_Code_R:h9:cANSI
    colorscheme solarized

elseif has('gui_gtk2') || has('gui_gtk3')
    set dir=/tmp,/var/tmp
    set undodir=/tmp,/var/tmp
    "set guifont=Anonymous\ Pro\ 9
    "set guifont=Anonymous\ Pro\ Bold\ 10
    "set guifont=Consolas\ 9
    "set guifont=DejaVu\ Sans\ Mono\ 9
    "set guifont=Envy\ Code\ R\ 9
    "set guifont=Fira\ Mono\ 10
    "set guifont=Fira\ Mono\ Medium\ 9
    "set guifont=Fira\ Mono\ Medium\ 8
    "set guifont=Inconsolata\ SemiBold\ 10
    "set guifont=Inconsolata\ Ultra-Bold\ 10
    "set guifont=Liberation\ Mono\ 10
    "set guifont=Monaco\ 10
    "set guifont=ProggyCleanTTSZBP\ 12
    "set guifont=PxPlus\ IBM\ VGA8\ 11
    "set guifont=PxPlus\ IBM\ VGA9\ 11
    "set guifont=PxPlus\ IBM\ MDA\ 11
    "set guifont=Source\ Code\ Pro\ 10
    "set guifont=Source\ Code\ Pro\ Medium\ 9
    "set guifont=Source\ Code\ Pro\ Semi-Bold\ 10
    "set guifont=Source\ Code\ Pro\ Semi-Bold\ 9
    "set guifont=Source\ Code\ Pro\ Semi-Bold\ 8
    "set guifont=Terminus\ 9
    "set guifont=Ubuntu\ Mono\ 10
    "set guifont=Ubuntu\ Mono\ Bold\ 10
    "set guifont=Droid\ Sans\ Mono\ Slashed\ Bold\ 8
    "set guifont=Input\ Mono\ Condensed\\,\ Medium\ Condensed\ 9
    "set guifont=Input\ Mono\ Narrow\ Medium\ 10
    "set guifont=Input\ Mono\ Compressed\ Medium\ Extra-Condensed\ 9
    "set guifont=Input\ Mono\ Condensed\\,\ Condensed\ 9
    "set guifont=Input\ Mono\ Compressed\ Medium\ Extra-Condensed\ 10
    set guifont=Cascadia\ Code\ 11
    "set guifont=Cascadia\ Mono\ 11
    "set guifont=Fantasque\ Sans\ Mono\ Bold\ 10
    "set guifont=Consolas\ Bold\ 10
    "set guifont=PxPlus\ IBM\ VGA9\ 12



    "colorscheme zenburn
    "colorscheme inkpot
    "colorscheme molokai
    "colorscheme elflord
    "colorscheme gruvbox
    "colorscheme monokai
    "color solarized
    "color one
    color gruvbox-material
    "color one
    "color darkblue

else
    set dir=/tmp,/var/tmp
    set undodir=/tmp,/var/tmp
    color gruvbox
endif

set background=dark

" Uncomment the following to have Vim jump to the last position
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" global option
" set autochdir exrc nomore
set exrc nomore
set cursorline
set encoding=utf-8
set fileencoding=utf-8
setglobal encoding=utf-8
setglobal fileencoding=utf-8
set scrolloff=3
nnoremap <F11> :YcmForceCompileAndDiagnostics <CR>
set visualbell "teaching self to use <Esc> properly
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set wildignorecase  " ignorecase during command completion
set nocursorcolumn
set virtualedit=onemore

" disable mouse
if has("gui_running")
	set mouse=a
else
	set mouse=
endif

set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2  " always show statusline
"set relativenumber
set number
set undofile
set redrawtime=2000
set foldmethod=manual

let g:airline_detect_modified=1
"let g:airline_theme="powerlineish"
"let g:airline_theme="palenight"
"let g:airline_theme="one"
let g:airline_theme='gruvbox_material'
"let g:airline_theme='edge'
let g:airline#extensions#tagbar#enabled=1

" completion
"
"set noshowfulltag
"set completeopt=menu,preview
"set wildmenu wildmode=list:full


" ignore Press ENTER or type command to continue
set shortmess=a
set cmdheight=2


" tabs
"set et
set ts=8
set sw=8
set softtabstop=8


" searching
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch


" handles long line correctly
set wrap
set textwidth=79
set formatoptions=qrn1
set colorcolumn=

set nolist
set listchars=eol:¬,tab:▶\ ,trail:~,extends:⟩,precedes:⟨,nbsp:␣
set showbreak=↳\ \ \ "

" insert current ISO date time when type xdate
iab xdate <c-r>=strftime("%Y-%m-%d %H:%M:%S")<cr>

" press F4 to switch between .cpp <-> .h file
" both must be in same directory
map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>

" fix vim to use perl/python regex
"nnoremap / /\v
"vnoremap / /\v

" to turn off search high light after you don't want them
" press <,><space>
let mapleader=','
nnoremap <leader><space> :noh<cr>
nnoremap <leader>l :set list!<cr>

"ctrl-p could initiate it
nnoremap <leader>cp :CtrlP<cr>
nnoremap <leader>cc :CtrlPClearAllCaches<cr>


"nnoremap <tab> %
"vnoremap <tab> %


nmap <F8> :TagbarToggle<CR>
nmap <F7> :SyntasticToggleMode<CR>

"nmap <leader>a= :Tabularize /=<CR>
"vmap <leader>a= :Tabularize /=<CR>
"nmap <leader>a: :Tabularize /:\zs<CR>
"vmap <leader>a: :Tabularize /:\zs<CR>

nmap <leader>a :Tabularize/
vmap <leader>a :Tabularize/

" disable Q mapping to avoid accidental *Ex mode*
nnoremap Q <nop>

" moves by screen line instead
nnoremap j gj
nnoremap k gk
xnoremap j gj
xnoremap k gk

nmap gh <C-w>h
nmap gj <C-w>j
nmap gk <C-w>k
nmap gl <C-w>l

" shortcut for tabnext and tabprevious
nmap <C-h> gT
nmap <C-j> gT
nmap <C-l> gt
nmap <C-k> gt


"set diffexpr=MyDiff()
"function MyDiff()
"    let opt = '-a --binary '
"    if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
"    if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
"    let arg1 = v:fname_in
"    if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
"    let arg2 = v:fname_new
"    if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
"    let arg3 = v:fname_out
"    if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
"    let eq = ''
"    if $VIMRUNTIME =~ ' '
"        if &sh =~ '\<cmd'
"            let cmd = '""' . $VIMRUNTIME . '\diff"'
"            let eq = '"'
"        else
"            let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
"        endif
"    else
"        let cmd = $VIMRUNTIME . '\diff'
"    endif
"    silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
"endfunction

" OLD auto command
"autocmd BufWritePre * retab
"autocmd BufWritePre * silent keepjumps %s/\s\+$//ge

"--------------------------------------------------------------------
" User defined commands

" Automatic updating of 'Last Modified:' headers
" autocmd BufWritePre * call s:ChangeDate()
" function! s:ChangeDate()
"     let regex = '\<Last Modified\s*:\s\zs.*'
"     let lnum = match(getline(1,10), regex)
"     if lnum >= 0
"         let existing = getline(lnum+1)
"         let substitute = strftime('%Y-%m-%d %X')
"         let updated = substitute(existing, regex, substitute, '')
"         if updated != existing
"             call setline(lnum+1, updated)
"         endif
"     endif
" endfunction

autocmd BufWritePre * call s:ReformatBuffer()
function! s:ReformatBuffer()
    echohl question
    echomsg 'Reformatting' expand('%:p:~')
    echohl none

    " use try/finally block to restore cursor position
    " interrupt by <C-c>
    let view = winsaveview()
    try
        set ff=unix

	" no BOM for UTF files
	set nobomb

        "strip trailing blanks
        "silent keepjumps %s/\s\+$//ge

        " strip trailing blanks including non-breakable space
        silent keepjumps %s/\(\s\|\%u00a0\)\+$//ge

        " re-tabulate everything (i.e. replace tab to space)
        " --> our setting here is 'expandtab'
        "retab

        " compact empty lines, to max of 3 empty lines
        silent keepjumps %s/\n\n\n\n\+/\r\r\r\r/ge

        " removes all CR
        silent keepjumps %s/\r//ge

    finally
        call winrestview(view)
    endtry
endfunction


" autosave when out focus, after all got persistent undo
" au FocusLost * :wa


" Control-Shift-PageUp drags the active tab page to the left (wraps around)
imap <silent> <C-S-PageUp> <C-o>:call <Sid>DragLeft()<Cr>
nmap <silent> <C-S-PageUp> :call <Sid>DragLeft()<Cr>
function! s:DragLeft()
    let n = tabpagenr()
    execute 'tabmove' (n == 1 ? '' : n - 2)
    let &showtabline = &showtabline
endfunction


" Control-Shift-PageDown drags the active tab page to the right (wraps around)
imap <silent> <C-S-PageDown> <C-o>:call <Sid>DragRight()<Cr>
nmap <silent> <C-S-PageDown> :call <Sid>DragRight()<Cr>
function! s:DragRight()
    let n = tabpagenr()
    execute 'tabmove' (n == tabpagenr('$') ? 0 : n)
    let &showtabline = &showtabline
endfunction


" Tab and Shift-Tab shift the indentation of the selected lines (from Visual Studio)
"xmap <Tab> >0gv
"smap <Tab> <C-o>V<C-o><Tab>
"xmap <S-Tab> <0gv
"smap <S-Tab> <C-o>V<C-o><S-Tab>
"imap <S-Tab> <C-o>V<C-o><S-Tab>

