# Vim Config

## Overview
Contains my personal gvim configurations. Includes:

* useful colorscheme: palenight, solarized, gruvbox, inkpot, palenight, molokai, zenburn, wombat, ..., etc.
* git plugin: fugitive
* ctrlP
* tagbar & taglist
* better html & javascript syntax indentation/highlighting
* pathogen (for loading commant-t git submodule)
* syntastic
* powerline
* youcompleteme

Also includes highlighting of mixed tab-space indentation.


## Requirements

For Python related (Debian based)
```
$ sudo apt-get install vim-gtk vim-doc vim-scripts python-pip
$ sudo pip install pyflakes pylint
```

For C++, need to recompile 'youcompleteme'.


## Installation
At your *HOME* folder, just do:

* `git clone git@bitbucket.org:dereks/vimconfig.git .vim`; or
* `git clone https://gitbucket.org/dereks/vimconfig.git .vim`

And then,

```
$ ln -s ~/.vim/.vimrc ~/.vimrc
$ cd .vim
~/.vim $ git submodule update --init --recursive
```

Please refer to [youcompleteme](https://vimawesome.com/plugin/youcompleteme) for its setup.


## Brief Usage

* `, ` (comma space) clear all highlighted searches
* `,a` tabularize
* `,cp` fuzzy-search file to open
* `,cc` clear file tree cache for fuzzy-search
* `,l` toggle show hidden char (tab/eol)
* `F4` swap open to related .h/.cpp file
* `F7` toggle syntastic
* `F8` toggle tagbar

